﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />
/// <reference path="../Scripts/kendo.web.js" />
myapp.BrowseDocuments.Document_render = function(element, contentItem) {
   
    var Document = kendo.data.Model.define({
        fields: {
            'Id': {
                editable: false,
                type: 'number'
            },
            'Name': {
                editable: true,
                type: 'string'
            }
        },
        id: 'Id'
    });

    var documentsDataSource = new kendo.data.DataSource({
        type: 'odata',
        transport: {
            read: {
                url: myapp.rootUri + '/ApplicationData.svc/Documents',
            },
            update: {
                url: getUrl,
            },
            destroy: {
                url: getUrl,
            },
            create: {
                url: myapp.rootUri + '/ApplicationData.svc/Documents',
            }
        },
        schema: {
            model: Document,
            data: 'value',
            total: function(response) {
                return response['odata.count'];
            }
        }
    });

    function getUrl(e) {
        console.log(e);
        return myapp.rootUri + '/ApplicationData.svc/Documents' + '(' + e.Id + ')';
    }
    

    var $documentsGrid = $('<div id="documentsGrid" />').appendTo($(element));
    $documentsGrid.kendoGrid({
        dataSource: documentsDataSource,
        toolbar: ["create", "save", "cancel"],
        editable: {
            confirmation: false
        },
        columns: [
            { field: 'Id', title: 'Id' },
            { field: 'Name', title: 'Name' },
            {
                command: ["destroy"]
            }
        ]
    });


};